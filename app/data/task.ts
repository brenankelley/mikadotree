import {IWorkItem} from "./iworkitem";

export class Task implements IWorkItem {
    constructor(id: number, title: string, description: string) {
        this.id = id;
        this.title = title;
        this.description = description;
    }

    readonly id: number;
    readonly title: string;
    readonly description: string;
}