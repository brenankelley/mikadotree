export class TreeEdge {
    constructor(from: number, to: number) {
        this.from = from;
        this.to = to;
    }

    readonly from: number;
    readonly to: number;
}