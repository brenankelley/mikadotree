import {IWorkItem} from "./iworkitem";
import {TreeEdge} from "./treeedge";

export class Tree {
    constructor(nodes: IWorkItem[], edges: TreeEdge[]) {
        this.nodes = nodes;
        this.edges = edges;
    }

    readonly nodes: IWorkItem[];
    readonly edges: TreeEdge[];
}
