export interface IWorkItem {
    readonly id: number;
    readonly title: string;
    readonly description: string;
}