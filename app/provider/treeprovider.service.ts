import {OpaqueToken} from "@angular/core";
import {Tree} from "../data/tree";

export interface TreeProviderService {
    getTree(): Promise<Tree>;
}
export const ITreeProviderService = new OpaqueToken("TreeProviderService");
