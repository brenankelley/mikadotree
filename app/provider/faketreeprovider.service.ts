import {Injectable} from "@angular/core";
import {Tree} from "../data/tree";
import {PBI} from "../data/pbi";
import {Task} from "../data/task";
import {TreeEdge} from "../data/treeedge";
import {TreeProviderService} from "./treeprovider.service";

@Injectable()
export class FakeTreeProviderService implements TreeProviderService {
    getTree(): Promise<Tree> {
        var nodes = [
            new PBI(1, "97756 Implement Circle in 3D View", "this is the description"),
            new Task(2, "Validate", "this is the description"),
            new Task(3, "Review Documentation", "this is the description"),
            new Task(4, "Update Documentation", "this is the description"),
            new Task(5, "Really Really Really Long Task Name That Is Still Very Important", "this is the description"),
        ];

        var edges = [
            new TreeEdge(1, 2),
            new TreeEdge(1, 3),
            new TreeEdge(3, 4),
            new TreeEdge(2, 5)
        ];

        return Promise.resolve(new Tree(nodes, edges));
    }
}