import {Component, Inject} from "@angular/core";
import {TreeProviderService, ITreeProviderService} from "./provider/treeprovider.service";
import {Tree} from "./data/tree";
import {IWorkItem} from "./data/iworkitem";
import {PBI} from "./data/pbi";
import * as vis from "vis";
import * as wordwrap from "wordwrap";

@Component({
    selector: 'my-app',
    template: '<div id="mikadotree"></div>',
})
export class MikadoTree {
    private readonly graphService: TreeProviderService;
    private network: vis.Network;
    private nodes: vis.DataSet;
    private edges: vis.DataSet;

    constructor(@Inject(ITreeProviderService) graphService: TreeProviderService) {
        this.graphService = graphService;
    }

    public ngAfterContentInit() {
        this.graphService.getTree().then(t => this.UpdateTree(t));
    }

    private static ConvertToVisualNode(node: IWorkItem): any {
        var wrap = wordwrap(25);
        var label = wrap(node.title);

        if (node instanceof PBI)
            return {id: node.id, label: label, group: "pbi"};
        return {id: node.id, label: label}
    }

    private UpdateTree(tree: Tree) {
        this.nodes = new vis.DataSet(tree.nodes.map(MikadoTree.ConvertToVisualNode));
        this.edges = new vis.DataSet(tree.edges);

        var container = document.getElementById('mikadotree');

        var data = {
            nodes: this.nodes,
            edges: this.edges
        };

        var groups = {
            blocked: {color: {background: 'red'}, borderWidth: 3},
            inprogress: {color: {background: 'yellow'}},
            pbi: {shape: 'box'}
        };

        var options = {
            autoResize: true,
            height: '100%',
            width: '100%',
            locale: 'en',
            clickToUse: false,
            layout: {
                improvedLayout: true,
                hierarchical: {
                    enabled: true,
                    levelSeparation: 300,
                    blockShifting: true,
                    edgeMinimization: true,
                    parentCentralization: true,
                    direction: 'LR',        // UD, DU, LR, RL
                    sortMethod: 'directed'   // hubsize, directed
                }
            },
            manipulation: {
                enabled: true,
                initiallyActive: true,
                addNode: MikadoTree.AddNode,
                addEdge: (d, c) => this.AddEdge(d, c),
                editNode: MikadoTree.EditNode,
                editEdge: MikadoTree.EditEdge,
                deleteNode: MikadoTree.DeleteNode,
                deleteEdge: MikadoTree.DeleteEdge,
            },
            nodes: {
            },
            edges: {
                arrows: 'to'
            },
            groups: groups,
            physics: {
                enabled: true,
                hierarchicalRepulsion: {
                    centralGravity: 0.1,
                    springLength: 200,
                    springConstant: 0.01,
                    nodeDistance: 100,
                    damping: 0.2
                },
            }
        };

        this.network = new vis.Network(container, data, options);
    }

    private static AddNode(data, callback) {
        callback(data);
    }

    private static EditNode(data, callback) {
        callback(data);
    }

    private static DeleteNode(data, callback) {
        callback(data);
    }

    private AddEdge(data, callback) {
        if (data.from == data.to || this.edges.get().find(e => e.from == data.from && e.to == data.to)) {
            callback(null);
            return;
        }

        if (this.edges.get().find(e => e.from == data.to && e.to == data.from)) {
            var edges: Array<any> = this.edges.get();
            var item = edges.find(e => e.from == data.to && e.to == data.from);
            this.edges.remove(item);
            this.edges.add(data);
        }
        callback(data);
    }

    private static EditEdge(data, callback) {
        callback(data);
    }

    private static DeleteEdge(data, callback) {
        callback(data);
    }
}