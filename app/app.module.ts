import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {MikadoTree} from "./app.mikadotree";
import {FakeTreeProviderService} from "./provider/faketreeprovider.service";
import {ITreeProviderService} from "./provider/treeprovider.service";

@NgModule({
    imports: [BrowserModule],
    declarations: [MikadoTree],
    bootstrap: [MikadoTree],
    providers: [
        {provide: ITreeProviderService, useClass: FakeTreeProviderService}
    ]
})
export class AppModule { }
